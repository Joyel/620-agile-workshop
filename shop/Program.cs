﻿namespace Shop;

public class Shop{
    public static void Main(string[] args){
        Console.WriteLine("Hello, Client!");

        bool doneShopping = false;
        int amount = 0;
        double cost = 0;
        double total = 0;
        string province = "";

        while(!doneShopping){
            bool isValid = false;
            while (!isValid){
                Console.WriteLine("Enter item amount");
                try{
                    amount = Convert.ToInt32(Console.ReadLine());
                    isValid = true;
                }
                catch{
                    Console.WriteLine("Please enter an int");
                }
            }

            isValid = false;

            
            while (!isValid){
                Console.WriteLine("Enter item cost");
                try{
                    cost = Convert.ToDouble(Console.ReadLine());
                    isValid = true;
                }
                catch{
                    Console.WriteLine("Please enter a double");
                }
            }

            
            isValid = false;

            while (!isValid){
                Console.WriteLine("Enter your province");
                    province = Console.ReadLine();
                    
                    if (province.Length != 2){
                        Console.WriteLine("Enter a 2-letter province code");
                    }
                    else{
                        isValid = true;
                    }
            }

            total += amount * cost;
            double discount = GetDiscount(total);
            Console.WriteLine(discount);
            total = total - (total * discount);

            double tax = GetTax(province.ToUpper());
            Console.WriteLine(tax);
            total = total + (total * tax);
            
            Console.WriteLine("Do you want to add another order? Y/N");
            string confirm = Console.ReadLine().ToUpper();
            if (confirm == "N"){
                doneShopping = true;
            }
        }

        
        // while (!isValid){
        //     Console.WriteLine("Enter item amount");
        //     try{
        //         amount = Convert.ToInt32(Console.ReadLine());
        //         isValid = true;
        //     }
        //     catch{
        //         Console.WriteLine("Please enter an int");
        //     }
        // }

        // isValid = false;

        
        // while (!isValid){
        //     Console.WriteLine("Enter item cost");
        //     try{
        //         cost = Convert.ToDouble(Console.ReadLine());
        //         isValid = true;
        //     }
        //     catch{
        //         Console.WriteLine("Please enter a double");
        //     }
        // }

        
        // isValid = false;

        // while (!isValid){
        //     Console.WriteLine("Enter your province");
        //         province = Console.ReadLine();
                
        //         if (province.Length != 2){
        //             Console.WriteLine("Enter a 2-letter province code");
        //         }
        //         else{
        //             isValid = true;
        //         }
        // }

        // double total = amount * cost;
        // double discount = GetDiscount(total);
        // Console.WriteLine(discount);
        // total = total - (total * discount);

        // double tax = GetTax(province.ToUpper());
        // Console.WriteLine(tax);
        // total = total + (total * tax);

        Console.WriteLine($"Your Total: {total}");
    }

    public static double GetDiscount(double orderAmount)
    {
        double discount = 0;

        if(orderAmount >= 1000 && orderAmount < 5000)
        {
            discount = 0.03;
        }
        else if(orderAmount >= 5000 && orderAmount < 7000)
        {
            discount = 0.05;
        }
        else if(orderAmount >= 7000 && orderAmount < 10000)
        {
            discount = 0.07;
        }
        else if(orderAmount >= 10000 && orderAmount < 50000)
        {
            discount = 0.1;
        }
        else if (orderAmount >= 50000)
        {
            discount = 0.15;
        }
        return discount;
    }

    public static double GetTax(string province)
    {
        double tax = 0;

        if(province == "QC")
        {
            tax = .14975;
        }
        else if(province == "BC")
        {
            tax = .12;
        }
        else if(province == "ON")
        {
            tax = .13;
        }
        else if(province == "PE")
        {
            tax = .15;
        }
        else
        {
            tax = .05;
        }
        return tax;
    }
}